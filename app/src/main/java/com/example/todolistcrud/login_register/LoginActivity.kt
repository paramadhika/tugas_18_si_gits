package com.example.todolistcrud.login_register

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.example.todolistcrud.R
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.Constant.Companion.PREF_ID_USER
import com.example.todolistcrud.helper.Constant.Companion.PREF_TOKEN
import com.example.todolistcrud.helper.Constant.Companion.PREF_USERNAME
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.ResponseUser
import com.example.todolistcrud.user.UserActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log


class LoginActivity : AppCompatActivity() {

    lateinit var sharedpref : PreferencesHelper
    private val api by lazy { ApiRetrofit().endpoint }
    private lateinit var loginEmail : EditText
    private lateinit var loginPass : EditText
    private lateinit var idUser : String
    private lateinit var username : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setLoginView()
        sharedpref = PreferencesHelper(this)

        setLoginListener()

        setSignupListener()
    }

    private fun setSignupListener() {
        signupfromlgn.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }


    private fun setLoginListener() {
        LoginBtn.setOnClickListener {
            if (loginEmail.text.isNotEmpty() && loginPass.text.isNotEmpty()){
                api.loginUser("User.php", "Login", loginEmail.text.toString(), loginPass.text.toString())
                    .enqueue(object : Callback<ResponseUser>{
                        override fun onResponse(
                            call: Call<ResponseUser>,
                            response: Response<ResponseUser>
                        ) {
                            if (response.isSuccessful){
                                for(item in response.body()!!.data!!){
                                    idUser = item!!.id.toString()
                                    username = item.firstname.toString() + " " + item.lastname.toString()
                                }
                                saveSessionLogin(loginEmail.text.toString(), loginPass.text.toString(), idUser, username)
                                Toast.makeText(
                                    applicationContext,
                                    "BERHASIL LOGIN!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                retriveToken()
                                LoginIntent()
                            }
                            else{
                                Toast.makeText(
                                    applicationContext,
                                    "GAGAL LOGIN!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Email atau Password Salah!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    })
            }
            else{
                Toast.makeText(
                    applicationContext,
                    "Masukkan Email & Password!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    @SuppressLint("StringFormatInvalid")
    private fun retriveToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->

            // Get new FCM registration token
            val token = task.result
            sharedpref.put(PREF_TOKEN, token.toString())

            //cek token
//            Toast.makeText(baseContext, "Token Saat ini : $token", Toast.LENGTH_LONG).show()
//            Log.d(TAG, "Token saat ini : $token")
        })
    }

    private fun saveSessionLogin(email: String, pass: String, id:String, userName: String){
        sharedpref.put(Constant.PREF_EMAIL, email)
        sharedpref.put(Constant.PREF_PASSWORD, pass)
        sharedpref.put(Constant.PREF_IS_LOGIN, true)
        sharedpref.put(PREF_ID_USER, id)
        sharedpref.put(PREF_USERNAME, userName)
    }

    private fun setLoginView() {
        loginEmail = findViewById(R.id.LoginEmail)
        loginPass = findViewById(R.id.LoginPass)
    }

    override fun onStart() {
        super.onStart()
        LoginIntent()
    }

    private fun LoginIntent(){
        if(sharedpref.getBoolean(Constant.PREF_IS_LOGIN)){
            username = sharedpref.geString(PREF_USERNAME).toString()
            Toast.makeText(
                applicationContext,
                "Selamat Datang ${username}",
                Toast.LENGTH_SHORT
            ).show()
            idUser = sharedpref.geString(PREF_ID_USER).toString()
            val intent = Intent(this, UserActivity::class.java)
                .putExtra("idUser", idUser)
            startActivity(intent)
            finish()
        }
    }
}