package com.example.todolistcrud.firebase

data class NotificationData(
    val title: String,
    val message : String
)