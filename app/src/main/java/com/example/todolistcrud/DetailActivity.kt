package com.example.todolistcrud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.api.NotificationRetrofit
import com.example.todolistcrud.firebase.PushNotification
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.ResponseNotification
import com.example.todolistcrud.model.SubmitTodo
import kotlinx.android.synthetic.main.activity_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private val todo by lazy { intent.getStringExtra("todo") }
    private val id by lazy { intent.getStringExtra("id") }
    private val todostatus by lazy { intent.getStringExtra("status") }
    private val created by lazy { intent.getStringExtra("created") }
    private val finish by lazy { intent.getStringExtra("finish") }
    lateinit var sharedpref: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        sharedpref = PreferencesHelper(this)

        setUpView()
        setEditListerner()
        setCompletedListener()
    }

    private fun setCompletedListener() {
        doneTodo.setOnClickListener {
            api.CompleteToDo("Todo.php", "updateTodo", id!!.toInt(), todo.toString(), 1)
                .enqueue(object : Callback<SubmitTodo>{
                    override fun onResponse(
                        call: Call<SubmitTodo>,
                        response: Response<SubmitTodo>
                    ) {
                        if (response.isSuccessful){
                            Toast.makeText(
                                applicationContext,
                                response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            onStart()
                        }
                        else {
                            Toast.makeText(
                                applicationContext,
                                response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Data Berhasil di Update!",
                            Toast.LENGTH_SHORT
                        ).show()
                        sendNotification()
                        finish()
                    }

                })
        }
    }

    private fun sendNotification() {
        api.pushNotification(
            "sendNotification.php",
            "pushNotification",
            sharedpref.geString(Constant.PREF_TOKEN).toString(),
        "You Have Completed Task : $todo")
            .enqueue(object : Callback<ResponseNotification>{
                override fun onResponse(
                    call: Call<ResponseNotification>,
                    response: Response<ResponseNotification>
                ) {
                    if (response.isSuccessful){
                        Toast.makeText(
                            applicationContext,
                            "Berhasil Mengirim Notifikasi",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else{
                        Toast.makeText(
                            applicationContext,
                            "Maaf Terjadi Kesalahan!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                override fun onFailure(call: Call<ResponseNotification>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "Gagal Mengirim Notifikasi!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun setEditListerner() {
        updateTodo.setOnClickListener {
            api.updateToDo("Todo.php", "updateTodo", id!!.toInt(), todoactivity.text.toString(), 0 )
                .enqueue(object : Callback<SubmitTodo>{
                    override fun onResponse(
                        call: Call<SubmitTodo>?,
                        response: Response<SubmitTodo>?
                    ) {
                        if (response!!.isSuccessful){
                            Toast.makeText(
                                applicationContext,
                                response!!.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            onStart()
                        }
                        else{
                            Toast.makeText(
                                applicationContext,
                                response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Data berhasil di Update!",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }
                })
        }
    }

    private fun setUpView() {
        val todoactivity : EditText = findViewById(R.id.todoactivity)
        val todoStatus : EditText = findViewById(R.id.todostatus)
        val dateCreated : EditText = findViewById(R.id.dateStart)
        val dateFinished : EditText = findViewById(R.id.dateFinish)


        todoactivity.setText(todo)
        if(todostatus == "1") todoStatus.setText("Sudah Selesai!")
        else todoStatus.setText("Belum Selesai")
        dateCreated.setText(created)
        dateFinished.setText(finish)
    }
}