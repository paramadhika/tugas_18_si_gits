package com.example.todolistcrud.api

import com.example.todolistcrud.helper.Constant.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NotificationRetrofit {

    companion object{
        private val retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        val Notifapi by lazy {
            retrofit.create(NotificationAPI::class.java)
        }
    }

}