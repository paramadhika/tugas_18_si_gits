package com.example.todolistcrud.api

import com.example.todolistcrud.firebase.PushNotification
import com.example.todolistcrud.helper.Constant.Companion.CONTENT_TYPE
import com.example.todolistcrud.helper.Constant.Companion.SERVER_KEY
import com.example.todolistcrud.model.ResponseGetTodoList
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface NotificationAPI {

    @Headers("Authorization: key=$SERVER_KEY", "Content-Type:$CONTENT_TYPE")
    @POST("fcm/send")
    suspend fun  postNotification(
        @Body notification: PushNotification
    ): Response<ResponseBody>
}