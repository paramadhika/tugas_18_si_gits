package com.example.todolistcrud.api

import com.example.todolistcrud.firebase.PushNotification
import com.example.todolistcrud.model.ResponseGetTodoList
import com.example.todolistcrud.model.ResponseNotification
import com.example.todolistcrud.model.ResponseUser
import com.example.todolistcrud.model.SubmitTodo
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface API {

    @GET("{data}/")
    fun getTodoList(
        @Path("data") data : String,
        @Query("function") function : String
    ): Call<ResponseGetTodoList>

    @GET("{data}/")
    fun getCompletedTodo(
        @Path("data") data : String,
        @Query("function") function : String
    ): Call<ResponseGetTodoList>

    @GET("{data}/")
    fun getUncompletedTodo(
        @Path("data") data : String,
        @Query("function") function : String
    ): Call<ResponseGetTodoList>

    @FormUrlEncoded
    @POST("{data}/")
    fun addTodoList(
        @Path("data") data : String,
        @Query("function") function : String,
        @Field("todo") todo : String
    ): Call<SubmitTodo>

    @FormUrlEncoded
    @POST("{data}/")
    fun updateToDo(
        @Path("data") data : String,
        @Query("function") function : String,
        @Query("id") id : Int,
        @Field("todo") todo : String,
        @Field("todoStatus") todoStatus : Int
    ): Call<SubmitTodo>

    @FormUrlEncoded
    @POST("{data}/")
    fun CompleteToDo(
        @Path("data") data : String,
        @Query("function") function : String,
        @Query("id") id : Int,
        @Field("todo") todo : String,
        @Field("todoStatus") todoStatus : Int
    ): Call<SubmitTodo>

    @DELETE("{data}/")
    fun deleteTodoList(
        @Path("data") data : String,
        @Query("function") function : String,
        @Query("id") id : Int
    ): Call<SubmitTodo>

    @GET("{data}/")
    fun getUserIdentity(
        @Path("data") data : String,
        @Query("function") function : String
    ): Call<ResponseUser>

    @FormUrlEncoded
    @POST("{data}/")
    fun regisNewUser(
        @Path("data") data : String,
        @Query("function") function : String,
        @Field("firstname") firstname: String,
        @Field("lastname") lastname : String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ResponseUser>

    @FormUrlEncoded
    @POST("{data}/")
    fun loginUser(
        @Path("data") data : String,
        @Query("function") function : String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ResponseUser>

    @FormUrlEncoded
    @POST("{data}/")
    fun updateUser(
        @Path("data") data : String,
        @Query("function") function : String,
        @Query("id") id : Int,
        @Field("firstname") firstname : String,
        @Field("lastname") lastname : String,
        @Field("email") email : String,
        @Field("password") password : String,
        @Field("image") image : String
    ): Call<ResponseUser>

    @Multipart
    @POST("{data}")
    fun uploadImage(
        @Path("data") data : String,
        @Query("function") function : String,
        @Part body: MultipartBody.Part
    ): Call<SubmitTodo>


    //Push Notification with PHP
    @FormUrlEncoded
    @POST("{data}")
    fun pushNotification(
        @Path("data") data : String,
        @Query("function") function : String,
        @Field("registration_id") registration_id : String,
        @Field("message") message : String
    ): Call<ResponseNotification>
}