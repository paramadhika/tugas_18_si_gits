package com.example.todolistcrud

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.todolistcrud.model.DataItem

class TodoListAdapter(val todo : ArrayList<DataItem>,
                      val listener : OnAdapterListener): RecyclerView.Adapter<TodoListAdapter.ViewHolder>(){
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val todoText : TextView = view.findViewById(R.id.ToDolist_item)
        val klik_delete : ImageButton = view.findViewById(R.id.deleteTodo)
        val klik_detail : CardView = view.findViewById(R.id.todo_klik)
        val dateCreated : TextView = view.findViewById(R.id.dateCreated)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListAdapter.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.todolist_item, parent,false)
    )

    override fun onBindViewHolder(holder: TodoListAdapter.ViewHolder, position: Int) {
        val todos = todo[position]
        holder.todoText.text = todos.todo
        holder.dateCreated.text = todos.dateCreated
        holder.klik_detail.setOnClickListener {
            listener.OnClick(todos)
        }
        holder.klik_delete.setOnClickListener {
            listener.OnDelete(todos)
        }
    }

    override fun getItemCount(): Int = todo.size

    fun setData(data : List<DataItem>){
        todo.clear()
        todo.addAll(data)
        notifyDataSetChanged()
    }

    interface OnAdapterListener{
        fun OnClick(dataTodo : DataItem)
        fun OnDelete(dataTodo: DataItem)
    }

}