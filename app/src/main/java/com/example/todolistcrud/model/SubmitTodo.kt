package com.example.todolistcrud.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubmitTodo(
    @field:SerializedName("message")
    val message : String? = null,

    @field:SerializedName("status")
    val status : Int? = null

) : Parcelable